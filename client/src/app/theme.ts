import {createTheme, Theme} from '@mui/material/styles';

const theme: Theme = createTheme({
    palette: {
        primary: {
            main: '#3a95c4',
        },
        secondary: {
            main: '#84b750',
        },
        background: {
            default: '#eeeeee'
        }
    },
    layout: {
        headerHeight: 100,
        commonChatWidth: 1280,
        chatMaxWidth: 500,
        chatMinWidth: 380,
    },
    pattern: {
        flex: {
            display: 'flex',
            alignItems: 'center'
        }
    }
});

export default theme;