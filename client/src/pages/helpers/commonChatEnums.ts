export enum Characters {
    Stan = '@Stan',
    Kyle = '@Kyle',
    Cartman = '@Cartman',
    Kenny = '@Kenny',
    Butters = '@Butters',
    Wendy = '@Wendy'
}