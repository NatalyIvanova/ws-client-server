/** @jsxImportSource @emotion/react */
import {Characters} from "./helpers/commonChatEnums";
import Chat from "../entities/Chat";
import Header from "../entities/Header";
import {Box} from "@mui/material";
import {useTheme} from "@emotion/react";
import SidebarToolbar from "../widgets/Sidebar/SidebarToolbar";

const styles = {
    commonChat: (theme) => ({
        height: '100vh',
        backgroundColor: theme.palette.background.default,
    }),
    mainArea: (theme) => ({
        display: 'flex',
        justifyContent: 'space-evenly',
        flexWrap: 'wrap',
        gap: theme.spacing(3),
        maxWidth: theme.layout.commonChatWidth,
        height: '100%',
        marginRight: 'auto',
        marginLeft: 'auto',
        paddingTop: `calc(${theme.layout.headerHeight}px + ${theme.spacing(3)})`,
        paddingRight: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        overflowY: 'auto'
    })
}

const CommonChat = () => {
    const compName = 'common-chat';
    const theme = useTheme();

    return (
        <Box className={compName} sx={styles.commonChat}>
            <Header />
            <SidebarToolbar />
            <Box className={`${compName}-main`} css={styles.mainArea(theme)}>
                { Object.keys(Characters).map(character =>  (
                    <Chat key={character} name={character}/>
                ))}
            </Box>
        </Box>
    );
}

export default CommonChat;