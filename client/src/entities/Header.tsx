/** @jsxImportSource @emotion/react */
import {useTheme} from "@emotion/react";
import {AppBar} from "@mui/material";
import {useMemo} from "react";

const styles = {
    headerHeight: (theme) => ({
        height: theme.layout.headerHeight,
        backgroundColor: theme.palette.common.white,
        backgroundImage: 'url("/natural-wooden-background.jpg")',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover'
    }),
    pageTitle: {
        textAlign: 'center',
        margin: 0
    },
    title: (theme) => ({
        height: theme.layout.headerHeight
    })
}

const Header = () => {
    const compName = 'header';
    const theme = useTheme();
    const imageName = useMemo(() => (`/logo.png`), []);

    return (
        <AppBar className={compName} css={styles.headerHeight(theme)}>
            <h1 className={`${compName}-pageTitle`} css={styles.pageTitle}>
                <img src={imageName} className={`${compName}-title`} css={styles.title(theme)} alt='South park'/>
            </h1>
        </AppBar>
    )
}

export default Header;