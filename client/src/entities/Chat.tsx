/** @jsxImportSource @emotion/react */
import {keyframes} from '@emotion/react';
import ChatForm from "../widgets/ChatForm";
import Messages from "../widgets/Messages";
import {useTheme} from "@emotion/react";
import {useMemo, useState} from "react";

const pulse = keyframes`
    0% {
        box-shadow: 0 0 0 0 rgba(132, 183, 80, 0.8);
    }
    70% {
        box-shadow: 0 0 0 14px rgba(132, 183, 80, 0);
    }
    100% {
        box-shadow: 0 0 0 0 rgba(132, 183, 80, 0);
    }
`

const styles = {
    chat: (theme) => ({
        flexBasis: '32%',
        maxWidth: theme.layout.chatMaxWidth,
        minWidth: theme.layout.chatMinWidth,
        padding: theme.spacing(3),
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[1]
    }),
    info: (theme) => ({
        textAlign: 'center',
        marginBottom: theme.spacing(2)
    }),
    avatar: (theme, newMessageReceived) => ({
        display: 'inline-block',
        width: '100px',
        height: '100px',
        padding: theme.spacing(0.75),
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: newMessageReceived ? theme.palette.secondary.main : theme.palette.grey[500],
        borderRadius: '50%',
        animationName: newMessageReceived && pulse,
        animationDuration: '1200ms',
        animationIterationCount: 3,
    }),
    name: (theme) => ({
        color: theme.palette.primary.main,
        margin: 0,
    })
}

const Chat = ({name}) => {
    const compName = 'chat';
    const theme = useTheme();
    const [newMessageReceived, setNewMessageReceived] = useState<boolean>(false);
    const imageName = useMemo(() => (`/characters/${name.toLowerCase()}.jpg`), [name]);
    let newMessageReceivedTimer = 0;

    const handleNewMessageReceived = () => {
        setNewMessageReceived(true);
        newMessageReceivedTimer = setTimeout(handleNoMessageReceived, 6000);
    }

    const handleNoMessageReceived = () => {
        setNewMessageReceived(false);
        if (handleNewMessageReceived) clearTimeout(newMessageReceivedTimer);
    }

    return (
        <div className={compName} css={styles.chat(theme)}>
            <div className={`${compName}-info`} css={styles.info(theme)}>
                <img src={imageName}
                     className={`${compName}-avatar`}
                     alt={`${name}'s avatar`}
                     css={styles.avatar(theme, newMessageReceived)}/>
                <h2 className={`${compName}-name`} css={styles.name(theme)}>{ name }</h2>
            </div>
            <ChatForm name={name}/>
            <Messages name={name} handleNewMessageReceived={handleNewMessageReceived}/>
        </div>
    );
}

export default Chat;