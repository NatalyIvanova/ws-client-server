/** @jsxImportSource @emotion/react */
import {useTheme} from "@emotion/react";
import {TechologyEnums} from "../widgets/Sidebar/toolbarEnums";

const styles = {
    itemsList: (theme) => ({
        padding: theme.spacing(1)
    }),
    title: (theme) => ({
        marginTop: 0,
        marginBottom: 0
    })
}

const ItemsList = ({list}) => {
    const compName = 'items-list';
    const theme = useTheme();
    const [name, displayName] = list;

    return (
        <div className={compName} css={styles.itemsList(theme)}>
            <h3 className={`${compName}-title`} css={styles.title(theme)}>
                { displayName }
            </h3>
            <ul className={`${compName}-list`}>
                {Object.values(TechologyEnums[name]).map(item =>
                    <li className={`${compName}-item`} key={item}>
                        {item}
                    </li>
                )}
            </ul>
        </div>
    )
}

export default ItemsList;