/** @jsxImportSource @emotion/react */
import {useMessages} from "./hooks/useMessages";
import {Message} from "./helpers/messagesEnums";
import {useTheme} from "@emotion/react";
import {useEffect} from "react";

const styles = {
    messages: (theme) => ({
        height: '280px',
        marginTop: theme.spacing(2),
        overflowY: 'auto'
    }),
    author: {
        fontWeight: 500
    },
    recipient: (theme) => ({
        fontWeight: 500,
        color: theme.palette.success.light,
    }),
    text: (theme) => ({
        color: theme.palette.grey[700],
        wordBreak: 'break-all'
    })
}

const Messages = ({name, handleNewMessageReceived}) => {
    const compName = 'messages';
    const theme = useTheme();
    const messages: Message[] = useMessages(name);

    useEffect(() => {
        if (messages.length) handleNewMessageReceived();
    }, [messages]);

    return (
        <div className={compName} css={styles.messages(theme)}>
            {messages?.map(({author, recipient = null, text}, i) => (
                <dl key={i}>
                    <dt className={`${compName}-author`} css={styles.author}>{ author }:</dt>
                    <dd className={`${compName}-text`} css={styles.text(theme)}>
                        <span className={`${compName}-recipient`} css={styles.recipient(theme)}>
                            { recipient && recipient}
                        </span>
                        { text }
                    </dd>
                </dl>
            ))}
        </div>
    )
}

export default Messages;