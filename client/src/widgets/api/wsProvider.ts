import {Message, wsClosedReason, wsReadyStates} from "../helpers/messagesEnums";

console.log(import.meta.env);

const wsUri = import.meta.env.VITE_WSS_URI || "ws://localhost:8080";
console.log(wsUri);
const wsProvider = (url) => {
    let client;
    let isConnected = false;
    let reconnectOnClose = true;
    let messageListeners = {};
    const stateChangeListeners = [];
    let restartCounter = 0;
    const maximumRestarts = 3;
    let handleRestart;

    const on = (compName, fn) => {
        messageListeners[compName] = fn;
    }

    const off = (compName, fn) => {
        delete messageListeners[compName];
    }

    const onStateChange = (fn) => {
        stateChangeListeners.push(fn);
        return () => {
            stateChangeListeners.filter(listener => listener !== fn);
        }
    }

    const restart = () => {
        if (restartCounter <= maximumRestarts) {
            clearTimeout(handleRestart);
            handleRestart = setTimeout(() => start(url), 3000);
        } else {
            clearTimeout(handleRestart);
            client.close();
            console.log('Maximum restarts was reached. Websocket connection is closed');
        }
    }

    const getReadyState = (clientReadyState) => {
        return Object.entries(wsReadyStates).find(([state, index]) => index === clientReadyState);
    }

    const getClosedReason = (e) => {
        const reason = wsClosedReason[e.code];
        return reason ? reason : "Unknown reason";
    }

    const handleIncoming = (incomingMessage: string) => {
        const formattedMessage: Message = JSON.parse(incomingMessage);
        if (formattedMessage.recipient) {
            messageListeners[formattedMessage.recipient.slice(1)](formattedMessage);
        } else {
            for (const listener in messageListeners) {
                messageListeners[listener](formattedMessage);
            }
        }
    }

    const start = (url) => {
        client = new WebSocket(url);
        console.log(client);

        client.onopen = () => {
            isConnected = true;
            stateChangeListeners.forEach(fn => fn(true));
        }

        const close = client.close;
        // Close without reconnecting;
        client.close = () => {
            console.log('ws was closed by app');
            reconnectOnClose = false;
            close.call(client);
        }

        const send = client.send;
        client.send = (outgoingMessage: Message) => {
            const clientReadyState = getReadyState(client.readyState);
            if (clientReadyState[1] as wsReadyStates === wsReadyStates.OPEN) {
                console.log('outgoingMessage', outgoingMessage);
                send.call(client, JSON.stringify(outgoingMessage));
            }
            else {
                console.log(
                    `WebSocket state is not "OPEN", but "${clientReadyState[0]}".
                     The message "${outgoingMessage}" was not sent.`
                );
            }
        }

        client.onmessage = (e) => {
            handleIncoming(e.data);
        }

        client.onerror = (e) => {
            console.error('ws error happened: ', e);
            restartCounter++;
        }

        client.onclose = (e) => {
            isConnected = false;
            stateChangeListeners.forEach(fn => fn(false));

            if (!reconnectOnClose) {
                console.log('ws closed by app');
                return;
            }

            console.log('ws closed by server. ', getClosedReason(e));
            restart();
        }
    }

    start(url);

    return {
        on,
        off,
        onStateChange,
        close: () => client.close(),
        isConnected: () => isConnected,
        send: (outgoingMessage) => client.send(outgoingMessage),
    }
}

export const wsClient = wsProvider(wsUri);
