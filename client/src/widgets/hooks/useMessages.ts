import {useEffect, useState} from "react";
import {wsClient} from "../api/wsProvider";
import {Message} from "../helpers/messagesEnums";
import {Characters} from "../../pages/helpers/commonChatEnums";

export const useMessages = (name: Characters) => {
    const [messages, setMessages] = useState<Message[]>([]);

    const handleMessage = (message: Message) => {
        setMessages([message, ...messages]);
    }

    useEffect(() => {
        wsClient.on(name, handleMessage);
        return () => wsClient.off(name, handleMessage);
    }, [messages]);

    return messages;
}
