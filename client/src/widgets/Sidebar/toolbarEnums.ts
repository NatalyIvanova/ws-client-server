export enum Technologies {
    server = 'Server side',
    client = 'Client Side',
    ui = 'UI Technologies'
}

export enum ServerSide {
    Nodejs = 'Node.js',
    Websocket = 'Websocket'
}

export enum ClientSide {
    React = 'React 18',
    Websocket = 'Websocket',
    Vite = 'Vite',
    Typescipt = 'Typescript',
    CustomHooks = 'Custom hooks',
    MemoizingValues = 'Memoizing values'
}

export enum UiTechnologies {
    Mui = 'MUI (Material UI)',
    Emotion = '@emotion',
    Animation = 'CSS Animation'
}

export const TechologyEnums = {
    server: ServerSide,
    client: ClientSide,
    ui: UiTechnologies,
}