/** @jsxImportSource @emotion/react */
import {useTheme} from "@emotion/react";
import SidebarTrigger from "./SidebarTrigger";
import {useState} from "react";
import SidebarPanel from "./SidebarPanel";

const styles = {
    toolbar: (theme, isSidebarOpened) => ({
        width: 400,
        height: '100vh',
        backgroundColor: theme.palette.background.default,
        boxShadow: theme.shadows[14],
        position: 'fixed',
        right: isSidebarOpened ? 0 : '-400px',
        zIndex: theme.zIndex.drawer,
        transition: 'right 380ms'
    })
}

const SidebarToolbar = () => {
    const compName = 'sidebar-toolbar';
    const theme = useTheme();
    const [isSidebarOpened, setSidebarOpened] = useState<boolean>(false);

    const toggleSidebar = (e) => {
        setSidebarOpened(!isSidebarOpened);
    }

    return (
        <div className={compName} css={styles.toolbar(theme, isSidebarOpened)}>
            <SidebarTrigger isSidebarOpened={isSidebarOpened} toggleSidebar={toggleSidebar}/>
            <SidebarPanel isSidebarOpened={isSidebarOpened} />
        </div>
    )
}

export default SidebarToolbar;