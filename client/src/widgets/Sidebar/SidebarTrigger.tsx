/** @jsxImportSource @emotion/react */
import {keyframes} from '@emotion/react';
import {useMemo} from "react";
import {IconButton} from "@mui/material";

const buttonWidth = 100;

const bounce = keyframes`
  0% {
    transform: rotate(-45deg);
  }
  5%{
    transform: rotate(0);
  }
  10% {
    transform: rotate(-45deg);
  }
  15%{
    transform: rotate(0);
  }
  20% {
    transform: rotate(-45deg);
  }
  100% {
    transform: rotate(-45deg);
  }
`

const styles = {
    trigger: {
        position: 'absolute',
        left: -buttonWidth
    },
    button: {
        display: 'flex',
        placeItems: 'center',
        width: buttonWidth,
        borderRadius: '50%',
        '&:hover': {
            backgroundColor: 'rgba(0, 0, 0, 0.1)'
        }
    },
    image: (isSidebarOpened) => ({
        width: buttonWidth - 10,
        animationName: !isSidebarOpened && bounce,
        animationDuration: '6000ms',
        animationIterationCount: 'infinite',
        transform: isSidebarOpened && 'rotate(-45deg)'
    })
}

const SidebarTrigger = ({isSidebarOpened, toggleSidebar}) => {
    const compName = 'sidebar-trigger';
    const imageName = useMemo(() => (`/characters/chef.png`), []);
    const description = isSidebarOpened ? 'Close Sidebar' : 'Open Sidebar';

    return (
        <div className={compName} css={styles.trigger}>
            <IconButton onClick={toggleSidebar}
                        aria-label={description}
                        className={`${compName}-button`}
                        css={styles.button}>
                <img src={imageName}
                     alt={description}
                     className={`${compName}-image`}
                     css={styles.image(isSidebarOpened)}/>
            </IconButton>
        </div>
    )
}

export default SidebarTrigger;