/** @jsxImportSource @emotion/react */
import {useTheme} from "@emotion/react";
import {Technologies} from "./toolbarEnums";
import ItemsList from "../../ui-components/ItemsList";

const styles = {
    panel: (theme) => ({
        padding: theme.spacing(3)
    }),
    title: (theme) => ({
        color: theme.palette.primary.main,
        marginTop: theme.spacing(3),
        marginBottom: 0,
    }),
    recipient: (theme) => ({
        fontWeight: 500,
        color: theme.palette.success.light,
        marginLeft: theme.spacing(0.5)
    }),
}

const SidebarPanel = () => {
    const compName = 'sidebar-panel';
    const theme = useTheme();

    return (
        <div className={compName} css={styles.panel(theme)}>
            <h2 className={`${compName}-title`} css={styles.title(theme)}>User Guide</h2>
            <p className={`${compName}-description`}>
                Address your message to some friend and start it with
                <span className={`${compName}-recipient`} css={styles.recipient(theme)}>
                    @YourFriendName
                </span>.
            </p>
            <p className={`${compName}-description`}>
                Message with no recipient's name will be broadcasted to all the friends in chat.
            </p>
            <p className={`${compName}-description`}>
                Websocket API will deliver your message instantly. Check Dev tools &#10143; Network &#10143; WS.
            </p>
            <h2 className={`${compName}-title`} css={styles.title(theme)}>Technologies</h2>
            { Object.entries(Technologies).map((list, i) => (
                <ItemsList key={i} list={list} />
            ))}
        </div>
    )
}

export default SidebarPanel;