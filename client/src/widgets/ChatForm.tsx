/** @jsxImportSource @emotion/react */
import {useMemo, useState} from "react";
import {wsClient} from "./api/wsProvider";
import {Message} from "./helpers/messagesEnums";
import {Characters} from "../pages/helpers/commonChatEnums";
import {useTheme} from "@emotion/react";
import {Button, TextField} from "@mui/material";
import {charatersPlaceholders} from "../entities/helpers/charactersPlaceholders";

const pattern = new RegExp(Object.values(Characters).join('|'));

const styles = {
    chartForm: (theme) => ({
        ...theme.pattern.flex,
    }),
    input: (theme) => ({
        flexGrow: 1
    }),
    submit: (theme) => ({
        height: '40px',
        marginLeft: theme.spacing(1.5),
    })
}

const ChatForm = ({name}) => {
    const compName = 'chat-form';
    const theme = useTheme();
    const [newMessage, setNewMessage] = useState<string>(charatersPlaceholders[name] || '');
    const inputLabel = useMemo(() => (`Message`), [name]);

    const handleInput = (e) => {
        setNewMessage(e.target.value);
    }

    const buildMessage = (message): Message => {
        const recipient = message.match(pattern);
        return recipient ?
            {
                author: name,
                recipient: recipient[0],
                text: message.replace(recipient[0], '')
            } :
            {
                author: name,
                recipient: null,
                text: message
            }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (newMessage) {
            wsClient.send(buildMessage(newMessage));
        }
        setNewMessage('');
    }

    return (
        <form className={compName} onSubmit={handleSubmit} css={styles.chartForm}>
            <TextField value={newMessage}
                       onChange={handleInput}
                       label={inputLabel}
                       variant='outlined'
                       size='small'
                       sx={styles.input(theme)} />
            <Button className={`${compName}-submit`}
                    type='submit'
                    variant='contained'
                    color='primary'
                    sx={styles.submit(theme)}>
                Send
            </Button>
        </form>
    )
}
export default ChatForm;